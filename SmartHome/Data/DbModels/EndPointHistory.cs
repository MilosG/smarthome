﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.DbModels
{
    public class EndPointHistory
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateUTC { get; set; }
        public string Description { get; set; }
        public int Value { get; set; }        

        [ForeignKey("EndPoint")]
        public int EndPointId { get; set; }
        public virtual EndPoint EndPoint { get; set; }
    }
}
