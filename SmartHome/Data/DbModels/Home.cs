﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.DbModels
{
    public class Home
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [ForeignKey("Creator")]
        public string CreatorId { get; set; }
        public virtual IdentityUser Creator { get; set; }
        public virtual ICollection<ApplicationUserHome> ApplicationUsersHomes { get; set; }
        public virtual ICollection<EndPoint> EndPoints { get; set; }       
    }
}
