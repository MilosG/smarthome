﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class EndPointHomeFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HomeId",
                table: "EndPoints",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_EndPoints_HomeId",
                table: "EndPoints",
                column: "HomeId");

            migrationBuilder.AddForeignKey(
                name: "FK_EndPoints_Homes_HomeId",
                table: "EndPoints",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EndPoints_Homes_HomeId",
                table: "EndPoints");

            migrationBuilder.DropIndex(
                name: "IX_EndPoints_HomeId",
                table: "EndPoints");

            migrationBuilder.DropColumn(
                name: "HomeId",
                table: "EndPoints");
        }
    }
}
