﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class EndPointState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TurnOn",
                table: "EndPoints");

            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "EndPoints",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "EndPoints");

            migrationBuilder.AddColumn<bool>(
                name: "TurnOn",
                table: "EndPoints",
                type: "bit",
                nullable: true);
        }
    }
}
