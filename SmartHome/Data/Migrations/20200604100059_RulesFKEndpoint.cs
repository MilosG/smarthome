﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class RulesFKEndpoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rules_Homes_HomeId",
                table: "Rules");

            migrationBuilder.AlterColumn<int>(
                name: "HomeId",
                table: "Rules",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "EndPointId",
                table: "Rules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Rules_EndPointId",
                table: "Rules",
                column: "EndPointId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rules_EndPoints_EndPointId",
                table: "Rules",
                column: "EndPointId",
                principalTable: "EndPoints",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rules_Homes_HomeId",
                table: "Rules",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rules_EndPoints_EndPointId",
                table: "Rules");

            migrationBuilder.DropForeignKey(
                name: "FK_Rules_Homes_HomeId",
                table: "Rules");

            migrationBuilder.DropIndex(
                name: "IX_Rules_EndPointId",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "EndPointId",
                table: "Rules");

            migrationBuilder.AlterColumn<int>(
                name: "HomeId",
                table: "Rules",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Rules_Homes_HomeId",
                table: "Rules",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
