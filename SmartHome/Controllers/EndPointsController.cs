﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SmartHome.Data;
using SmartHome.Data.DbModels;
using SmartHome.Models;
using SmartHome.Service.Interfaces;

namespace SmartHome.Controllers
{
    [Authorize]
    public class EndPointsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IEndPointService _endPointService;
        private readonly IRuleService _ruleService;

        public EndPointsController(ApplicationDbContext context, IEndPointService endPointService, IRuleService ruleService)
        {
            _context = context;
            _endPointService = endPointService;
            _ruleService = ruleService;
        }

        private string GetLoggedInUserId()
        {
            var id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            return id;
        }

        public async Task<IActionResult> AddRuleToEndPoint(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            return View(new Rule() { EndPointId = id.Value, RuleType = Data.Enumerables.RuleTypeEnum.TimeBased });
        }
        [HttpPost]
        public async Task<IActionResult> AddRuleToEndPoint([Bind("Name,RuleType,ValueFrom,ValueTo,TimeFrom,TimeTo,EndPointId")]  Rule rule)
        {
            _ruleService.CreateRule(rule, GetLoggedInUserId());
            return RedirectToAction("ManageRulesForEndPoint", new { id = rule.EndPointId });
        }

        public async Task<IActionResult> ManageRulesForEndPoint(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            return View(CastRulesToViewModel(id.Value, _endPointService.GetRulesForEndPoint(id.Value, GetLoggedInUserId()).ToList()));
        }

        private EndPointRulesTableViewModel CastRulesToViewModel(int endPointId, List<Rule> list)
        {
            var isCreator = false;
            var homeId = _endPointService.GetHomeIdForEndPoint(endPointId).Value;
            if (list.Any())
                isCreator = _ruleService.IsUserCreator(list.FirstOrDefault().Id, GetLoggedInUserId());
            var items = list.Select(sl => new EndPointRuleViewModel
            {
                Id = sl.Id,
                Name = sl.Name,
                RuleType = sl.RuleType,
                TimeFrom = sl.TimeFrom,
                TimeTo = sl.TimeTo,
                ValueFrom = sl.ValueFrom,
                ValueTo = sl.ValueTo
            }).ToList();
            return new EndPointRulesTableViewModel
            {
                IsUserCreator = isCreator,
                EndPointId = endPointId,
                HomeId = homeId,
                Items = items
            };
        }

        // GET: EndPoints/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var endPoint = _endPointService.GetEndPointById(id.Value, GetLoggedInUserId());
            if (endPoint == null)
            {
                return NotFound();
            }
            return View(endPoint);
        }

        // POST: EndPoints/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,Type,HomeId")] EndPoint endPoint)
        {
            if (id != endPoint.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _endPointService.UpdateEndPoint(endPoint, GetLoggedInUserId());
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EndPointExists(endPoint.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Homes", new { id = endPoint.HomeId });
            }
            return View(endPoint);
        }

        // GET: EndPoints/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var endPoint = _endPointService.GetEndPointById(id.Value, GetLoggedInUserId());
            if (endPoint == null)
            {
                return NotFound();
            }

            return View(endPoint);
        }

        // POST: EndPoints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var endpoint = _endPointService.GetEndPointById(id, GetLoggedInUserId());
            _endPointService.DeleteEndPoint(id, GetLoggedInUserId());
            return RedirectToAction("Details", "Homes", endpoint.HomeId);
        }

        private bool EndPointExists(int id)
        {
            return _context.EndPoints.Any(e => e.Id == id);
        }
    }
}
