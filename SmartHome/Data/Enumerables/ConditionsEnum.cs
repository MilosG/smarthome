﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.Enumerables
{   
    public enum RuleTypeEnum
    {
        TimeBased = 1,
        ValueBased = 2
    }
}
