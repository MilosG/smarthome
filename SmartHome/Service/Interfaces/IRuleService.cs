﻿using SmartHome.Data.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Service.Interfaces
{
    public interface IRuleService
    {
        Rule GetRuleById(int id);
        void DeleteRule(int id, string userId);
        void CreateRule(Rule rule, string userId);
        void UpdateRule(Rule rule, string userId);
        bool RuleExists(int id);        
        bool CanUserAccessRule(int ruleId, string userId);
        bool IsUserCreator(int ruleId, string userId);
    }
}
