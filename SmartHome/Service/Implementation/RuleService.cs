﻿using Microsoft.EntityFrameworkCore;
using SmartHome.Data;
using SmartHome.Data.DbModels;
using SmartHome.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Service.Implementation
{
    public class RuleService : IRuleService
    {
        private readonly ApplicationDbContext _context;
        private readonly IHomeService _homeService;
        private readonly IEndPointService _endPointService;
        public RuleService(ApplicationDbContext context, IHomeService homeService, IEndPointService endPointService)
        {
            _context = context;
            _endPointService = endPointService;
            _homeService = homeService;
        }

        public bool CanUserAccessRule(int ruleId, string userId)
        {
            var rule = _context.Rules.FirstOrDefault(fr => fr.Id == ruleId);
            return _endPointService.CanUserAccesEndPoint(rule.EndPointId, userId);
        }

        public void CreateRule(Rule rule, string userId)
        {
            if (_endPointService.CanUserAccesEndPoint(rule.EndPointId, userId))
            {
                _context.Add(rule);
                SaveChanges();
            }
        }

        public void DeleteRule(int id, string userId)
        {
            if (IsUserCreator(id, userId))
            {
                var rule = GetRuleById(id);
                _context.Rules.Remove(rule);
                SaveChanges();
            }
        }        

        public Rule GetRuleById(int id)
        {
            return _context.Rules.FirstOrDefault(fr => fr.Id == id);
        }

        public bool IsUserCreator(int ruleId, string userId)
        {
            var rule = _context.Rules.FirstOrDefault(fr => fr.Id == ruleId);

            return _endPointService.IsUserCreator(rule.EndPointId, userId);
        }

        public bool RuleExists(int id)
        {
            return _context.Rules.Any(e => e.Id == id);
        }

        public void UpdateRule(Rule rule, string userId)
        {
            _context.Update(rule);
            SaveChanges();
        }

        private void SaveChanges()
        {
            _context.SaveChanges();

        }
    }
}
