﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class newValuesToRules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Operator",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "Rules");

            migrationBuilder.AddColumn<int>(
                name: "HoursFrom",
                table: "Rules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "HoursTo",
                table: "Rules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinutesFrom",
                table: "Rules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinutesTo",
                table: "Rules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RuleType",
                table: "Rules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ValueFrom",
                table: "Rules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ValueTo",
                table: "Rules",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HoursFrom",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "HoursTo",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "MinutesFrom",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "MinutesTo",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "RuleType",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "ValueFrom",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "ValueTo",
                table: "Rules");

            migrationBuilder.AddColumn<int>(
                name: "Operator",
                table: "Rules",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Value",
                table: "Rules",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
