﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.Enumerables
{
    public enum EndPointTypeEnum
    {
        Lights = 1,
        Heat = 2,
        Lock = 3
    }
}
