﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class removedFKRuleHOme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rules_Homes_HomeId",
                table: "Rules");

            migrationBuilder.DropIndex(
                name: "IX_Rules_HomeId",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "HomeId",
                table: "Rules");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HomeId",
                table: "Rules",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rules_HomeId",
                table: "Rules",
                column: "HomeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rules_Homes_HomeId",
                table: "Rules",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
