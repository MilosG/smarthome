﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class timeFromTimetochanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HoursFrom",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "HoursTo",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "MinutesFrom",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "MinutesTo",
                table: "Rules");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "TimeFrom",
                table: "Rules",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "TimeTo",
                table: "Rules",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeFrom",
                table: "Rules");

            migrationBuilder.DropColumn(
                name: "TimeTo",
                table: "Rules");

            migrationBuilder.AddColumn<int>(
                name: "HoursFrom",
                table: "Rules",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "HoursTo",
                table: "Rules",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinutesFrom",
                table: "Rules",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinutesTo",
                table: "Rules",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
