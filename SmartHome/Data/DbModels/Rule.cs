﻿using SmartHome.Data.Enumerables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.DbModels
{
    public class Rule
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }        
        public RuleTypeEnum RuleType { get; set; }
        public int ValueFrom { get; set; }
        public int ValueTo { get; set; }
        public TimeSpan TimeFrom { get; set; }
        public TimeSpan TimeTo { get; set; }
        
        [ForeignKey("EndPoint")]
        public int EndPointId { get; set; }

        public virtual EndPoint EndPoint { get; set; }
    }
}
