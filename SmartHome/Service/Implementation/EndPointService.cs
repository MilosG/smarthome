﻿using Microsoft.EntityFrameworkCore;
using SmartHome.Data;
using SmartHome.Data.DbModels;
using SmartHome.Data.Enumerables;
using SmartHome.Models;
using SmartHome.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Service.Implementation
{
    public class EndPointService : IEndPointService
    {
        private readonly ApplicationDbContext _context;
        private readonly IHomeService _homeService;
        public EndPointService(ApplicationDbContext context, IHomeService homeService)
        {
            _context = context;
            _homeService = homeService;
        }

        public bool AddEndPointToHome(AddEndPointToHomeViewModel model, string userId)
        {
            var endpoint = _context.EndPoints.FirstOrDefault(fr => fr.Address == model.EndPointMAC);
            if (endpoint == null || !_homeService.CanUserAccessHome(model.HomeId, userId))
                return false;

            endpoint.HomeId = model.HomeId;
            _context.Update(endpoint);
            _context.SaveChanges();
            return true;
        }

        public bool? AddNewValueToEndpoint(string address, int value)
        {
            var endpoint = GetEndPointByAddress(address);
            if (endpoint == null)
            {
                return null;
            }
            endpoint.PointHistories.Add(new EndPointHistory()
            {
                DateUTC = DateTime.UtcNow,
                Description = "New value added",
                EndPointId = endpoint.Id,
                Value = value
            });
            _context.SaveChanges();

            var turnOn = false;
            if (endpoint.Rules.Any())
                foreach (var rule in endpoint.Rules)
                {
                    if (!turnOn)
                        if (rule.RuleType == Data.Enumerables.RuleTypeEnum.TimeBased)
                        {
                            var utcNow = DateTime.UtcNow;
                            var time = new TimeSpan(utcNow.Hour, utcNow.Minute, utcNow.Second);
                            if (time >= rule.TimeFrom && time <= rule.TimeTo)
                                turnOn = true;
                        }
                        else
                        {
                            if (value >= rule.ValueFrom && value <= rule.ValueTo)
                                turnOn = true;
                        }
                }
            return turnOn;
        }

        public bool CanUserAccesEndPoint(int endpointId, string userId)
        {
            var endpoint = _context.EndPoints.FirstOrDefault(fr => fr.Id == endpointId);
            return _homeService.CanUserAccessHome(endpoint.HomeId.Value, userId);
        }

        public bool ChangeEndPointState(int endpointId, EndPointStateEnum stateEnum, string userId)
        {
            var endpoint = GetEndPointById(endpointId, userId);
            if (endpoint == null)
                return false;
            endpoint.State = stateEnum;
            _context.SaveChanges();
            return true;
        }

        public void CreateEndPoint(EndPoint endPoint)
        {
            endPoint.HomeId = null;
            _context.Add(endPoint);
            _context.SaveChanges();
        }

        public void DeleteEndPoint(int id, string userId)
        {
            _context.EndPoints.Remove(GetEndPointById(id, userId));
            _context.SaveChanges();
        }

        public EndPoint GetEndPointByAddress(string address)
        {
            return _context.EndPoints.Include(i => i.PointHistories).Include(i => i.Rules).FirstOrDefault(fr => fr.Address.Equals(address));
        }

        public EndPoint GetEndPointById(int id, string userId)
        {
            var endPoint = _context.EndPoints.FirstOrDefault(m => m.Id == id);
            if (!_homeService.CanUserAccessHome(endPoint.HomeId.Value, userId))
                return null;
            return endPoint;
        }

        public int? GetHomeIdForEndPoint(int id)
        {
            return _context.EndPoints.FirstOrDefault(fr => fr.Id == id).HomeId;
        }

        public IEnumerable<Rule> GetRulesForEndPoint(int endpointId, string userId)
        {
            var endpoint = _context.EndPoints.Include(i => i.Rules).FirstOrDefault(fr => fr.Id == endpointId);
            if (!_homeService.CanUserAccessHome(endpoint.HomeId.Value, userId))
                return null;
            return endpoint.Rules;
        }

        public bool IsUserCreator(int endpointId, string userId)
        {
            var endpoint = _context.EndPoints.FirstOrDefault(fr => fr.Id == endpointId);
            return _homeService.IsUserCreator(endpoint.HomeId.Value, userId);
        }

        public bool? RegisterNewDevice(string address, int type)
        {
            if (_context.EndPoints.FirstOrDefault(fr => fr.Address.Equals(address)) == null)
            {
                _context.EndPoints.Add(
                    new EndPoint()
                    {
                        Address = address,
                        Home = null,
                        Name = "",
                        Type = (Data.Enumerables.EndPointTypeEnum)type
                    });
            }
            _context.SaveChanges();
            return null;
        }

        public void UpdateEndPoint(EndPoint endPoint, string userId)
        {
            if (_homeService.CanUserAccessHome(endPoint.HomeId.Value, userId))
            {
                _context.Update(endPoint);
                _context.SaveChanges();
            }
        }
    }
}
