﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartHome.Data;
using SmartHome.Data.DbModels;
using SmartHome.Service.Interfaces;

namespace SmartHome.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class EndPointsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IEndPointService _endPointService;


        public EndPointsController(ApplicationDbContext context, IEndPointService endPointService)
        {
            _endPointService = endPointService;
            _context = context;
        }


        // GET: api/EndPoints1/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EndPoint>> GetEndPoint(int id, string userId)
        {
            var endPoint = _endPointService.GetEndPointById(id,userId);

            if (endPoint == null)
            {
                return NotFound();
            }

            return endPoint;
        }

        // PUT: api/EndPoints1/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEndPoint(int id, EndPoint endPoint)
        {
            if (id != endPoint.Id)
            {
                return BadRequest();
            }

            _context.Entry(endPoint).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EndPointExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EndPoints1
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<EndPoint>> PostEndPoint(EndPoint endPoint)
        {
            _endPointService.CreateEndPoint(endPoint);

            return CreatedAtAction("GetEndPoint", new { id = endPoint.Id }, endPoint);
        }

        // DELETE: api/EndPoints1/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<EndPoint>> DeleteEndPoint(int id, string userId)
        {
            var endPoint = _endPointService.GetEndPointById(id, userId);
            if (endPoint == null)
            {
                return NotFound();
            }

            _endPointService.DeleteEndPoint(id, userId);

            return endPoint;
        }

        private bool EndPointExists(int id)
        {
            return _context.EndPoints.Any(e => e.Id == id);
        }
    }
}
