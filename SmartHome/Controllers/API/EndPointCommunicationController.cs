﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartHome.Service.Interfaces;

namespace SmartHome.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class EndPointCommunicationController : ControllerBase
    {
        private readonly IEndPointService _endPointService;

        public EndPointCommunicationController(IEndPointService endPointService)
        {
            _endPointService = endPointService;
        }
        
        [HttpGet("NewValue/{address}/{value}")]
        public bool? NewValue(string address, int value)
        {
            return _endPointService.AddNewValueToEndpoint(address, value);            
        }

        [HttpGet("RegisterDevice/{address}/{type}")]
        public bool? RegisterDevice(string address, int type)
        {
            return _endPointService.RegisterNewDevice(address, type);
        }
    }
}