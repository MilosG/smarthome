﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SmartHome.Data;
using SmartHome.Data.DbModels;
using SmartHome.Service.Interfaces;

namespace SmartHome.Controllers
{
    public class RulesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IRuleService _ruleService;

        public RulesController(ApplicationDbContext context, IRuleService ruleService)
        {
            _ruleService = ruleService;
            _context = context;
        }

        private string GetLoggedInUserId()
        {
            var id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            return id;
        }
        // GET: Rules/Create
        public IActionResult Create()
        {
            ViewData["EndPointId"] = new SelectList(_context.EndPoints, "Id", "Id");
            return View();
        }

        // POST: Rules/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,RuleType,ValueFrom,ValueTo,TimeFrom,TimeTo,EndPointId")] Rule rule)
        {
            if (ModelState.IsValid)
            {
                _ruleService.CreateRule(rule, GetLoggedInUserId());
                return RedirectToAction("Details","Homes",new { id = rule.EndPoint.HomeId});
            }            
            return View(rule);
        }

        // GET: Rules/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rule = _ruleService.GetRuleById(id.Value);
            if (rule == null)
            {
                return NotFound();
            }
            ViewData["EndPointId"] = new SelectList(_context.EndPoints, "Id", "Id", rule.EndPointId);
            return View(rule);
        }

        // POST: Rules/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, Name,RuleType,ValueFrom,ValueTo,TimeFrom,TimeTo,EndPointId")] Rule rule)
        {
            if (id != rule.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _ruleService.UpdateRule(rule, GetLoggedInUserId());
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RuleExists(rule.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("ManageRulesForEndPoint","EndPoints", new { id = rule.EndPointId });
            }            
            return View(rule);
        }

        // GET: Rules/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rule = _ruleService.GetRuleById(id.Value);
            if (rule == null)
            {
                return NotFound();
            }

            return View(rule);
        }

        // POST: Rules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rule = _ruleService.GetRuleById(id);
            _ruleService.DeleteRule(id, GetLoggedInUserId());
            return RedirectToAction("ManageRulesForEndPoint", "EndPoints", new { id = rule.EndPointId });
        }

        private bool RuleExists(int id)
        {
            return _ruleService.RuleExists(id);
        }
    }
}
