﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SmartHome.Data;
using SmartHome.Data.DbModels;
using SmartHome.Data.Enumerables;
using SmartHome.Models;
using SmartHome.Service.Interfaces;

namespace SmartHome.Controllers
{
    [Authorize]
    public class HomesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHomeService _homeService;
        private readonly IEndPointService _endPointService;

        public HomesController(ApplicationDbContext context, IHomeService homeService, IEndPointService endPointService)
        {
            _context = context;
            _endPointService = endPointService;
            _homeService = homeService;
        }
        private string GetLoggedInUserId()
        {
            var id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            return id;
        }
        public PartialViewResult ReloadEndPointsTable(int homeId)
        {
            var model = _context.EndPoints.Where(wh => wh.HomeId == homeId).ToList();
            return PartialView("_EndPointsTable", model);
        }

        //GET: ManageUsersInHome
        public async Task<IActionResult> DeleteUserFromHome(int homeId, string email)
        {
            _homeService.DeleteUserFromHome(homeId, email, GetLoggedInUserId());
            return RedirectToAction("ManageUsersInHome", new { id = homeId });
        }
        //GET: ManageUsersInHome
        public async Task<IActionResult> ManageUsersInHome(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (_homeService.GetHomeById(id.Value, GetLoggedInUserId()) == null)
                return RedirectToAction("Index", "Home", null);
            return View(new ManageUsersViewModel() { HomeId = id.Value, Users = _homeService.GetUsersForHome(id.Value, GetLoggedInUserId()) });
        }

        //GET: AddUserToHome
        public async Task<IActionResult> AddUserToHome(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            return View(new AddUserToHomeViewModel() { HomeId = id.Value, Email = "" });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUserToHome([Bind("HomeId,Email")] AddUserToHomeViewModel model)
        {
            if (!_homeService.AddUserToHome(model, GetLoggedInUserId()))
            {
                ModelState.AddModelError("Email", "Email not found");
                return View(model);
            }
            return RedirectToAction("ManageUsersInHome", new { id = model.HomeId });
        }

        public async Task<IActionResult> AddEndPointToHome(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            return View(new AddEndPointToHomeViewModel() { HomeId = id.Value, EndPointMAC = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEndPointToHome([Bind("HomeId,EndPointMAC")] AddEndPointToHomeViewModel model)
        {
            if (!_endPointService.AddEndPointToHome(model, GetLoggedInUserId()))
            {
                ModelState.AddModelError("EndPointMAC", "EndPoint MAC address not found, endpoint has to be registered first");
                return View(model);
            }
            return RedirectToAction("Details", new { id = model.HomeId });
        }

        // GET: Homes
        public async Task<IActionResult> Index()
        {
            return View(_homeService.GetUserHomes(GetLoggedInUserId()));
        }

        // GET: Homes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var home = _homeService.GetHomeById(id.Value, GetLoggedInUserId());
            if (home == null)
            {
                return NotFound();
            }

            var model =
                new HomeViewModel()
                {
                    EndPoints = home.EndPoints.Select(sl => new EndPointsViewModel()
                    {
                        Name = sl.Name,
                        Address = sl.Address,
                        Id = sl.Id,
                        Type = sl.Type,
                        State = sl.State
                    }).ToList(),
                    Creator = home.CreatorId,
                    Id = home.Id,
                    IsUserCreator = home.CreatorId == GetLoggedInUserId(),
                    Name = home.Name,                    
                    Actions = new List<SelectListItem>{
                        new SelectListItem { Text = EndPointStateEnum.ON.ToString(), Value = ((int) EndPointStateEnum.ON).ToString()},
                        new SelectListItem { Text = EndPointStateEnum.OFF.ToString(), Value = ((int) EndPointStateEnum.OFF).ToString()},
                        new SelectListItem { Text = EndPointStateEnum.AUTO.ToString(), Value = ((int) EndPointStateEnum.AUTO).ToString()}
                    }
                };
            return View(model);
        }

        [HttpPost]
        public JsonResult ChangeEndPointState(int id, int state)
        {            
            return Json(_endPointService.ChangeEndPointState(id, (EndPointStateEnum)state, GetLoggedInUserId()));
        }

        // GET: Homes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Homes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] Home home)
        {
            if (ModelState.IsValid)
            {
                home = _homeService.CreateHome(home, GetLoggedInUserId());

                return RedirectToAction(nameof(Index));
            }
            return View(home);
        }

        // GET: Homes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var home = _homeService.GetHomeById(id.Value, GetLoggedInUserId());
            if (home == null)
            {
                return NotFound();
            }
            return View(home);
        }

        // POST: Homes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Home home)
        {
            if (id != home.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _homeService.UpdateHome(home, GetLoggedInUserId());
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HomeExists(home.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(home);
        }

        // GET: Homes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var home = _homeService.GetHomeById(id.Value, GetLoggedInUserId());
            if (home == null)
            {
                return NotFound();
            }

            return View(home);
        }

        // POST: Homes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _homeService.DeleteHome(id, GetLoggedInUserId());
            return RedirectToAction(nameof(Index));
        }

        private bool HomeExists(int id)
        {
            return _context.Homes.Any(e => e.Id == id);
        }
    }
}
