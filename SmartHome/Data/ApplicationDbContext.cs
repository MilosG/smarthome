﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SmartHome.Data.DbModels;

namespace SmartHome.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<EndPoint>().HasIndex(i => i.Address).IsUnique();
            base.OnModelCreating(builder);
        }        

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }        
        public DbSet<ApplicationUserHome> UserHomes { get; set; }
        public DbSet<EndPoint> EndPoints { get; set; }
        public DbSet<EndPointHistory> EndPointHistories { get; set; }
        public DbSet<Home> Homes { get; set; }        
        public DbSet<Rule> Rules { get; set; }
    }
}
