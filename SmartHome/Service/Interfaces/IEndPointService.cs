﻿using SmartHome.Data.DbModels;
using SmartHome.Data.Enumerables;
using SmartHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Service.Interfaces
{
    public interface IEndPointService
    {
        bool? AddNewValueToEndpoint(string address, int value);
        EndPoint GetEndPointById(int id, string userId);
        EndPoint GetEndPointByAddress(string address);
        void CreateEndPoint(EndPoint endPoint);

        void UpdateEndPoint(EndPoint endPoint, string userId);

        void DeleteEndPoint(int id, string userId);

        bool AddEndPointToHome(AddEndPointToHomeViewModel model, string userId);
        bool? RegisterNewDevice(string address, int type);
        IEnumerable<Rule> GetRulesForEndPoint(int endpointId, string userId);
        int? GetHomeIdForEndPoint(int id);
        bool CanUserAccesEndPoint(int endpointId, string userId);
        bool IsUserCreator(int endpointId, string userId);

        bool ChangeEndPointState(int endpointId, EndPointStateEnum stateEnum, string userId);

    }
}
