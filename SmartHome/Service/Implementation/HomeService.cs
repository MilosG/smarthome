﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SmartHome.Data;
using SmartHome.Data.DbModels;
using SmartHome.Models;
using SmartHome.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Service.Implementation
{
    public class HomeService : IHomeService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public HomeService(ApplicationDbContext context, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        public Home CreateHome(Home home, string userId)
        {
            home.CreatorId = userId;
            _context.Add(home);
            _context.SaveChanges();

            _context.UserHomes.Add(new ApplicationUserHome() { HomeId = home.Id, ApplicationUserId = userId });
            _context.SaveChanges();
            return home;
        }

        public void DeleteHome(int id, string userId)
        {
            if (IsUserCreator(id, userId))
            {
                var home = GetHomeById(id);
                if (home != null)
                {
                    _context.Homes.Remove(home);
                    _context.SaveChanges();
                }
            }

        }

        public HomeViewTableModel GetUserHomes(string userId)
        {
            var items = _context.Homes.Where(wh => wh.ApplicationUsersHomes.Any(an => an.ApplicationUserId == userId)).Select(sl => new HomeViewModel() { Creator = sl.Creator.Email, Id = sl.Id, Name = sl.Name, IsUserCreator = sl.CreatorId == userId }).ToList();
            return new HomeViewTableModel() { Items = items };
        }

        public void UpdateHome(Home home, string userId)
        {
            if (CanUserAccessHome(home.Id, userId))
            {
                _context.Update(home);
                _context.SaveChanges();
            }
        }

        public Home GetHomeById(int id, string userId)
        {
            if (CanUserAccessHome(id, userId))
            {
                var home = _context.Homes.Include(i => i.Creator).Include(i => i.EndPoints).FirstOrDefault(m => m.Id == id);
                return home;
            }
            return null;
        }

        public bool AddUserToHome(AddUserToHomeViewModel model, string userId)
        {
            var user = _userManager.FindByEmailAsync(model.Email).Result;
            if (user == null)
                return false;
            if (IsUserCreator(model.HomeId, userId))
            {
                _context.UserHomes.Add(new ApplicationUserHome() { HomeId = model.HomeId, ApplicationUserId = user.Id });
                _context.SaveChanges();
                return true;
            }
            return false;

        }

        public List<User> GetUsersForHome(int homeId, string userId)
        {
            if (!CanUserAccessHome(homeId, userId))
                return null;
            return _context.UserHomes.Where(wh => wh.HomeId == homeId).Select(sl => new User() { Email = sl.ApplicationUser.Email }).Distinct().ToList();
        }

        public void DeleteUserFromHome(int homeId, string email, string userId)
        {
            if (IsUserCreator(homeId, userId))
            {
                var user = _userManager.FindByEmailAsync(email).Result;

                var items = _context.UserHomes.Where(wh => wh.HomeId == homeId && wh.ApplicationUserId == user.Id).ToList();
                foreach (var i in items)
                {
                    _context.Remove(i);
                }
                _context.SaveChanges();
            }
        }
        private Home GetHomeById(int homeId)
        {
            return _context.Homes.Include(i=> i.ApplicationUsersHomes).FirstOrDefault(fr => fr.Id == homeId);
        }
        public bool CanUserAccessHome(int homeId, string userId)
        {
            var home = GetHomeById(homeId);
            if (home.ApplicationUsersHomes.Any(an => an.ApplicationUserId == userId))
                return true;
            return false;
        }

        public bool IsUserCreator(int homeId, string userId)
        {
            return _context.Homes.FirstOrDefault(fr => fr.Id == homeId).CreatorId == userId;
        }
    }
}
