﻿using SmartHome.Data.DbModels;
using SmartHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Service.Interfaces
{
    public interface IHomeService
    {
        HomeViewTableModel GetUserHomes(string userId);
        Home GetHomeById(int id, string userId);

        Home CreateHome(Home home, string userId);

        void UpdateHome(Home home, string userId);

        void DeleteHome(int id, string userId);

        bool AddUserToHome(AddUserToHomeViewModel model, string userId);

        List<User> GetUsersForHome(int homeId, string userId);

        void DeleteUserFromHome(int homeId, string email, string userId);
        bool CanUserAccessHome(int homeId, string userId);
        bool IsUserCreator(int homeId, string userId);
    }
}
