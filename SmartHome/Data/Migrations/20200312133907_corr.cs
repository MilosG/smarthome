﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class corr : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_USerHomes_AspNetUsers_ApplicationUserId",
                table: "USerHomes");

            migrationBuilder.DropForeignKey(
                name: "FK_USerHomes_Homes_HomeId",
                table: "USerHomes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_USerHomes",
                table: "USerHomes");

            migrationBuilder.RenameTable(
                name: "USerHomes",
                newName: "UserHomes");

            migrationBuilder.RenameIndex(
                name: "IX_USerHomes_HomeId",
                table: "UserHomes",
                newName: "IX_UserHomes_HomeId");

            migrationBuilder.RenameIndex(
                name: "IX_USerHomes_ApplicationUserId",
                table: "UserHomes",
                newName: "IX_UserHomes_ApplicationUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserHomes",
                table: "UserHomes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserHomes_AspNetUsers_ApplicationUserId",
                table: "UserHomes",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserHomes_Homes_HomeId",
                table: "UserHomes",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserHomes_AspNetUsers_ApplicationUserId",
                table: "UserHomes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserHomes_Homes_HomeId",
                table: "UserHomes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserHomes",
                table: "UserHomes");

            migrationBuilder.RenameTable(
                name: "UserHomes",
                newName: "USerHomes");

            migrationBuilder.RenameIndex(
                name: "IX_UserHomes_HomeId",
                table: "USerHomes",
                newName: "IX_USerHomes_HomeId");

            migrationBuilder.RenameIndex(
                name: "IX_UserHomes_ApplicationUserId",
                table: "USerHomes",
                newName: "IX_USerHomes_ApplicationUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_USerHomes",
                table: "USerHomes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_USerHomes_AspNetUsers_ApplicationUserId",
                table: "USerHomes",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_USerHomes_Homes_HomeId",
                table: "USerHomes",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
