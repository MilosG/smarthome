﻿using SmartHome.Data.Enumerables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Models
{
    public class EndPointViewModel
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public EndPointTypeEnum Type { get; set; }
        public int HomeId { get; set; }
        public string Name { get; set; }
    }

    public class EndPointRulesTableViewModel
    {
        public int HomeId { get; set; }
        public int EndPointId { get; set; }
        public bool IsUserCreator { get; set; }
        public List<EndPointRuleViewModel> Items { get; set; }
    }

    public class EndPointRuleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public RuleTypeEnum RuleType { get; set; }
        public int ValueFrom { get; set; }
        public int ValueTo { get; set; }
        public TimeSpan TimeFrom { get; set; }
        public TimeSpan TimeTo { get; set; }
    }
}
