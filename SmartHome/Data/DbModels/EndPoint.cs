﻿using SmartHome.Data.Enumerables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.DbModels
{
    public class EndPoint
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public EndPointTypeEnum Type { get; set; }
        public EndPointStateEnum State { get; set; }
        [ForeignKey("Home")]
        public int? HomeId { get; set; }

        public virtual Home Home { get; set; }
        public virtual ICollection<EndPointHistory> PointHistories { get; set; }
        public virtual ICollection<Rule> Rules { get; set; }
    }
}
