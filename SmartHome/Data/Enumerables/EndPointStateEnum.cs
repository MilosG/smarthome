﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.Enumerables
{
    public enum EndPointStateEnum
    {
       ON = 1,
       OFF = 2,
       AUTO = 3
    }
}
