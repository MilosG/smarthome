﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SmartHome.Data.Enumerables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Models
{
    public class HomeViewTableModel{
        public List<HomeViewModel> Items { get; set; }
    }
    public class HomeViewModel {
        public int Id { get; set; }
        public string Name { get; set; }        
        public string Creator { get; set; }   
        public bool IsUserCreator { get; set; }            
        public List<SelectListItem> Actions { get; set; } 
        public List<EndPointsViewModel> EndPoints { get; set; }
    }    
    public class EndPointsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public EndPointStateEnum State { get; set; }
        public EndPointTypeEnum Type { get; set; }                        
    }

    public class AddUserToHomeViewModel
    {
        public int HomeId { get; set; }
        public string Email { get; set; }
    }
    public class AddEndPointToHomeViewModel
    {
        public int HomeId { get; set; }
        public string EndPointMAC { get; set; }
    }

    public class ManageUsersViewModel
    {
        public int HomeId { get; set; }      
        public List<User> Users { get; set; }        
    }
    public class User
    {
        public string Email { get; set; }        
    }
}
