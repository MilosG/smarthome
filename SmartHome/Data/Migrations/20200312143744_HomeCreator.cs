﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class HomeCreator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatorId",
                table: "Homes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Homes_CreatorId",
                table: "Homes",
                column: "CreatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Homes_AspNetUsers_CreatorId",
                table: "Homes",
                column: "CreatorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Homes_AspNetUsers_CreatorId",
                table: "Homes");

            migrationBuilder.DropIndex(
                name: "IX_Homes_CreatorId",
                table: "Homes");

            migrationBuilder.DropColumn(
                name: "CreatorId",
                table: "Homes");
        }
    }
}
