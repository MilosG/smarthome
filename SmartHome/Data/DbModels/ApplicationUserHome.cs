﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHome.Data.DbModels
{
    public class ApplicationUserHome
    {
        [Key]
        public int Id{ get; set; }

        [ForeignKey("ApplicationUser")]
        public string ApplicationUserId { get; set; }
        public virtual IdentityUser ApplicationUser { get; set; }


        [ForeignKey("Home")]
        public int HomeId { get; set; }
        public virtual Home Home { get; set; }
    }
}
