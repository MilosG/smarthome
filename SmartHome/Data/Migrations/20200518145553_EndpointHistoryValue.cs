﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartHome.Data.Migrations
{
    public partial class EndpointHistoryValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId1",
                table: "UserHomes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Value",
                table: "EndPointHistories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserHomes_ApplicationUserId1",
                table: "UserHomes",
                column: "ApplicationUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_UserHomes_AspNetUsers_ApplicationUserId1",
                table: "UserHomes",
                column: "ApplicationUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserHomes_AspNetUsers_ApplicationUserId1",
                table: "UserHomes");

            migrationBuilder.DropIndex(
                name: "IX_UserHomes_ApplicationUserId1",
                table: "UserHomes");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId1",
                table: "UserHomes");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "EndPointHistories");
        }
    }
}
